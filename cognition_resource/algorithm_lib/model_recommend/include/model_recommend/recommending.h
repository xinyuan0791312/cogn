#ifndef RECOMMENDING_H
#define RECOMMENDING_H

#include <vector>
#include <string>
#include <map>
#include <ros/package.h>

struct task_description
{
    std::string task_type;
    std::string image_type;
    int image_width;
    int image_height;
    float cpu_mem_limit_gb;
    float gpu_mem_limit_gb;
    float time_limit_ms;
    bool is_dark;
};

struct task_vector
{
    char task_type;
    char image_type;
    char is_dark;
    float long_edge;
    float short_edge;
    float cpu_mem_limit_gb;
    float gpu_mem_limit_gb;
    float time_limit_ms;
};

struct model_vector
{
    char task_type;
    char image_type;
    char is_dark_applicable;
    float long_edge;
    float short_edge;
    float cpu_mem_cost_gb;
    float gpu_mem_cost_gb;
    float time_cost_ms;
    float map; //mAP
};

std::map<std::string, model_vector> load_model_list();
task_vector vectorize_task(const task_description &task);
std::vector<std::string> get_recommended_models(const task_vector &task, const std::map<std::string, model_vector> &model_list);

#endif // RECOMMENDING_H
