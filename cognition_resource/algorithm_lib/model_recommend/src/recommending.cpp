#include <iostream>
#include <fstream>
#include <algorithm>
#include <queue>
#include <cmath>
#include <dirent.h>
#include <tinyxml2.h>
#include <model_recommend/recommending.h>

using namespace tinyxml2;

struct model_description
{
    std::string model_id;
    std::string task_type;
    std::string image_type;
    int image_width;
    int image_height;
    float cpu_mem_cost_gb;
    float gpu_mem_cost_gb;
    float time_cost_ms;
    bool is_dark_applicable;
    float map;
};

static float distance(const task_vector &a, const model_vector &b)
{
    float scale1 = a.long_edge / b.long_edge;
    float scale2 = a.short_edge / b.short_edge;
    return (1.0f - b.map) * (1.0f - b.map) + std::log(std::min(scale1, scale2)) * std::log(std::min(scale1, scale2));
}

static std::vector<model_description> get_model_list(std::string model_dir)
{
    std::vector<model_description> result;
    DIR *p_dir;
    dirent *p_item;
    p_dir = opendir(model_dir.c_str());
    if(p_dir == NULL)
    {
        std::cerr << "Loading directory " << model_dir << " failed." << std::endl;
        return result;
    }
    p_item = readdir(p_dir);
    while(p_item != NULL)
    {
        const char *filename = p_item->d_name;
        int len = std::strlen(filename);
        if(len >= 4 && std::strcmp(filename + (len - 4), ".xml") == 0)
        {
            XMLDocument *model_xml = new XMLDocument();
            XMLError error = model_xml->LoadFile((model_dir + "/" + filename).c_str());
            if (error != XML_SUCCESS)
            {
                std::cerr << "Loading file " << filename << " failed." << std::endl;
                delete model_xml;
                closedir(p_dir);
                return result;
            }
            XMLElement *model_node = model_xml->RootElement();
            model_description temp;
            temp.model_id = model_node->Attribute("model_id");
            temp.task_type = model_node->Attribute("task_type");
            temp.image_type = model_node->Attribute("image_type");
            temp.image_width = atoi(model_node->Attribute("image_width"));
            temp.image_height = atoi(model_node->Attribute("image_height"));
            temp.cpu_mem_cost_gb = atof(model_node->Attribute("cpu_mem_cost_gb"));
            temp.gpu_mem_cost_gb = atof(model_node->Attribute("gpu_mem_cost_gb"));
            temp.time_cost_ms = atof(model_node->Attribute("time_cost_ms"));
            temp.is_dark_applicable = std::strcmp(model_node->Attribute("is_dark_applicable"), "true") == 0;
            temp.map = atof(model_node->Attribute("map"));
            result.push_back(temp);
            delete model_xml;
	    }
        p_item = readdir(p_dir);
    }
    closedir(p_dir);
    return result;
}

static void update_model_csv()
{
    std::string package_path = ros::package::getPath("model_recommend");
    std::string model_data_path = package_path + "/model_descriptions";
    std::vector<model_description> model_list = get_model_list(model_data_path);
    std::ofstream oss(model_data_path + "/model_descriptions.csv");
    for(int i = 0; i < model_list.size(); i++)
    {
        oss << model_list[i].model_id << ","
        << (model_list[i].task_type == "classification" ? 0 : (model_list[i].task_type == "detection" ? 1 : 2)) << ","
        << (model_list[i].image_type == "colored" ? 0 : (model_list[i].image_type == "infrared" ? 1 : (model_list[i].image_type == "visible" ? 2 : 3))) << ","
        << (model_list[i].is_dark_applicable ? 1 : 0) << ","
        << model_list[i].image_width << ","
        << model_list[i].image_height << ","
        << model_list[i].cpu_mem_cost_gb << ","
        << model_list[i].gpu_mem_cost_gb << ","
        << model_list[i].time_cost_ms << ","
        << model_list[i].map << std::endl;
    }
    oss.close();
}

task_vector vectorize_task(const task_description &task)
{
    task_vector result;
    result.task_type = task.task_type == "classification" ? 0 : (task.task_type == "detection" ? 1 : 2);
    result.image_type = task.image_type == "colored" ? 0 : (task.image_type == "infrared" ? 1 : (task.image_type == "visible" ? 2 : 3));
    result.is_dark = task.is_dark ? 1 : 0;

    result.long_edge = task.image_width > task.image_height ? task.image_width : task.image_height;
    result.short_edge = task.image_width < task.image_height ? task.image_width : task.image_height;
    result.cpu_mem_limit_gb = task.cpu_mem_limit_gb;
    result.gpu_mem_limit_gb = task.gpu_mem_limit_gb;
    result.time_limit_ms = task.time_limit_ms;
    return result;
}

std::vector<std::string> get_recommended_models(const task_vector &task, const std::map<std::string, model_vector> &model_list)
{
    std::vector<std::string> result_list;
    int model_length = model_list.size();
    //get the result
    for(std::map<std::string, model_vector>::const_iterator iter = model_list.begin(); iter != model_list.end(); iter++)
    {
        if(task.task_type == iter->second.task_type || (task.task_type == 1 && iter->second.task_type == 2))
        {
            if(task.image_type == iter->second.image_type && task.cpu_mem_limit_gb >= iter->second.cpu_mem_cost_gb && task.gpu_mem_limit_gb >= iter->second.gpu_mem_cost_gb)
            {
                if(!task.is_dark || iter->second.is_dark_applicable)
                {
                    result_list.push_back(iter->first);
                }
            }
        }
    }
    //sort the result, mao pao
    int result_length = result_list.size();
    for(int i = 0; i < result_length; i++)
    {
        for(int j = 0; j < result_length - i - 1; j++)
        {
            if(distance(task, model_list.at(result_list[j])) > distance(task, model_list.at(result_list[j+1])))
            {
                std::string temp = result_list[j];
                result_list[j] = result_list[j+1];
                result_list[j+1] = temp;
            }
        }
    }
    return result_list;
}

//update and get model_descriptions from csv
std::map<std::string, model_vector> load_model_list()
{
    update_model_csv();
    std::map<std::string, model_vector> result;
    std::string package_path = ros::package::getPath("model_recommend");
    std::string model_data_path = package_path + "/model_descriptions";
    FILE *file = std::fopen((model_data_path + "/model_descriptions.csv").c_str(), "r");
    int c = std::fgetc(file);
    while(c != EOF)
    {
        model_vector model;
        std::vector<char> buffer;
        std::string model_id;
        while(c != ',')
        {
            buffer.push_back((char)c);
            c = std::fgetc(file);
        }
        c = std::fgetc(file);
        buffer.push_back((char)0);
        model_id = buffer.data();
        buffer.clear();
        while(c != ',')
        {
            buffer.push_back((char)c);
            c = std::fgetc(file);
        }
        c = std::fgetc(file);
        buffer.push_back((char)0);
        model.task_type = atoi(buffer.data());
        buffer.clear();
        while(c != ',')
        {
            buffer.push_back((char)c);
            c = std::fgetc(file);
        }
        c = std::fgetc(file);
        buffer.push_back((char)0);
        model.image_type = atoi(buffer.data());
        buffer.clear();
        while(c != ',')
        {
            buffer.push_back((char)c);
            c = std::fgetc(file);
        }
        c = std::fgetc(file);
        buffer.push_back((char)0);
        model.is_dark_applicable = atoi(buffer.data());
        buffer.clear();
        while(c != ',')
        {
            buffer.push_back((char)c);
            c = std::fgetc(file);
        }
        c = std::fgetc(file);
        buffer.push_back((char)0);
        model.long_edge = atof(buffer.data());
        buffer.clear();
        while(c != ',')
        {
            buffer.push_back((char)c);
            c = std::fgetc(file);
        }
        c = std::fgetc(file);
        buffer.push_back((char)0);
        model.short_edge = atof(buffer.data());
        buffer.clear();
        while(c != ',')
        {
            buffer.push_back((char)c);
            c = std::fgetc(file);
        }
        c = std::fgetc(file);
        buffer.push_back((char)0);
        model.cpu_mem_cost_gb = atof(buffer.data());
        buffer.clear();
        while(c != ',')
        {
            buffer.push_back((char)c);
            c = std::fgetc(file);
        }
        c = std::fgetc(file);
        buffer.push_back((char)0);
        model.gpu_mem_cost_gb = atof(buffer.data());
        buffer.clear();
        while(c != ',')
        {
            buffer.push_back((char)c);
            c = std::fgetc(file);
        }
        c = std::fgetc(file);
        buffer.push_back((char)0);
        model.time_cost_ms = atof(buffer.data());
        buffer.clear();
        while(c != '\n')
        {
            buffer.push_back((char)c);
            c = std::fgetc(file);
        }
        c = std::fgetc(file);
        buffer.push_back((char)0);
        model.map = atof(buffer.data());
        buffer.clear();
        result[model_id] = model;
    }
    std::fclose(file);
    return result;
}
