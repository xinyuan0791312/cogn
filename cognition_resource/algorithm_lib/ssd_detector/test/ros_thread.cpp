#include "ros_thread.h"
#include <iostream>

RosThread::RosThread()
{
//    if (!ros::master::check())
//    {
//        return false;
//    }
    ros::start(); // explicitly needed since our nodehandle is going out of scope.

    m_isExt = false;
    m_ros_thread = NULL;
    m_ros_thread = new boost::thread(boost::bind(&RosThread::rosrunThread, this));
}

RosThread::~RosThread()
{
    m_isExt = true;
    if(m_ros_thread)
    {
        m_ros_thread->interrupt();
        m_ros_thread->join();
        delete m_ros_thread;
    }
    ROS_INFO("Ros shutdown, proceeding to close the gui.");
}

void RosThread::rosrunThread()
{
    ros::Duration initDur(0.2);
    while (ros::ok() && !m_isExt)
    {
        ros::spinOnce();
        initDur.sleep();
    }
    ROS_INFO("ros thread closing...");
}